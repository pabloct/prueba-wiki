
# Prueba Wiki

## Lo básico

Esto es una wiki de *prueba* para el grupo [**UCASE**](https://ucase.uca.es).

![UCA](https://gabcomunicacion.uca.es/wp-content/uploads/2017/05/1045848721_84201092222.png)

> I think we can put our differences behind us. For science. You monster.

### Listas 
1. Wikis de GitLab vs Repositorio
2. Notación **Markdown**
3. Secciones del repositorio
4. Colaboración mediante **merge requests**
	1. Ramas
	2. Merge requests

---
	
- Wikis de GitLab vs Repositorio
- Notación **Markdown**
- Secciones del repositorio
- Colaboración mediante **merge requests**
	- Ramas
	- Merge requests

---

1. - [x] Wikis de GitLab vs Repositorio
2. - [ ] Notación **Markdown**
3. - [ ] Secciones del repositorio
4. - [ ] Colaboración mediante **merge requests**
	1. - [ ] Ramas
	2. - [ ] Merge requests

---
### Código

`print("Hello, World")`

```c
int main(int argc, const char * argv[]) {
// insert code here...
std::cout << "Hello, World!\n";
return 0;
}
```

```json
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```

--- 
### Enlaces
Each view controller manages a view hierarchy, the root view of which is stored in the [view](https://developer.apple.com/documentation/uikit/uiviewcontroller/1621460-view, "UIView documentation") property of this class. 

Each view controller manages a view hierarchy, the root view of which is stored in the [view][1] property of this class. 

[1]: <https://developer.apple.com/documentation/uikit/uiviewcontroller/1621460-view> "UIView documentation"

---
### Tablas

| Cabecera 1 | Cabecera 2 |
| -----      | ------ |
| Celda 1 | Celda 2 |
| Celda 3 | Celda 4 |

## Extensiones
### Expresiones matemáticas

```math
a^2+b^2=c^2
```

### Diagramas

```mermaid
sequenceDiagram
	participant Alice
	participant Bob
    Alice->>John: Hello John, how are you?
    loop Healthcheck
        John->>John: Fight against hypochondria
    end
    Note right of John: This is a note!
    John-->>Alice: Great!
    John->>Bob: How about you?
    Bob-->>John: Jolly good!
```

```mermaid
graph LR
A[Square Rect] -- Link text --> B((Circle))
A --> C(Round Rect)
B --> D{Rhombus}
C --> D
```